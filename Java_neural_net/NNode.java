import java.util.*;
/**
 * Write a description of class NNode here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NNode
{
    // instance variables - replace the example below with your own
    private String name;
    //private double weight;
    private double bias;
    private HashMap<NNode, Double> connection;
    private double activation;
    private double in;
    private double delta;

    /**
     * Constructor for objects of class NNode
     */
    public NNode(String n, double a)
    {
        // initialise instance variables
        name = n;
        connection = new HashMap();
        activation = a;
        in = 0;
        delta = 0;
    }

    /**
     * Impleaments a connection between another node
     * <b>Preconditions</b>:        The both nodes have been created                  
     * <b>Postconditions</b>:       The connection has been created
     * 
     * @param  connection   a NNode containing the node to be connected
     * @param  weight       a double containning the weight of the connection
     */
    public void setConnection(NNode connection, double weight)
    {
        this.connection.put(connection, weight);
    }

    /**
     * Sets the bias weight of the node
     * <b>Preconditions</b>:        The node has been created                  
     * <b>Postconditions</b>:       None
     * 
     * @param  weight   a double containing the bias weight
     */
    public void setBiasWeight(double weight)
    {
        bias = weight;
    }
    
    /**
     * Returns the bias weight of the node
     * <b>Preconditions</b>:        The node has been created, and the bias weight has been set                
     * <b>Postconditions</b>:       None
     * 
     * @return  bias    a double containing the bias weight
     */
    public double getBiasWeight()
    {
        return bias;
    }

    /**
     * Sets the activation variable
     * <b>Preconditions</b>:        The node has been created                
     * <b>Postconditions</b>:       None
     * 
     * @param  a    a double containing the activation value
     */
    public void setActivation(double a)
    {
        activation = a;
    }

    /**
     * Returns the activation value
     * <b>Preconditions</b>:        The node has been created, and the activation value has been set                 
     * <b>Postconditions</b>:       None
     * 
     * @return  activation  a double containing the activation value
     */
    public double getActivation()
    {
        return activation;
    }

    /**
     * Returns all of the connections of the node 
     * <b>Preconditions</b>:        The node has been created and has connections                
     * <b>Postconditions</b>:       None
     * 
     * @return  connection.keySet() a Set containing all of the connections of the node 
     */
    public Set getNodes()
    {
        return connection.keySet();
    }

    /**
     * Returns all of the weight of the node connected to a different node
     * <b>Preconditions</b>:        The node has been created, and its weight has been set              
     * <b>Postconditions</b>:       None
     * 
     * @param  node a NNode containing the node found in the hashmap
     * @return  connection.get(node)    a double containing the weight of the node
     */
    public double getWeight(NNode node)
    {
        return connection.get(node);
    }
    
    /**
     * Stores the in(j) summation 
     * <b>Preconditions</b>:        The neural net has been created             
     * <b>Postconditions</b>:       None
     * 
     * @param  inJ  a double containing a summation
     */
    public void setIn(double in)
    {
        this.in = in;
    }
    
    /**
     * Returns the in(j) summation 
     * <b>Preconditions</b>:        The neural net has been created, and the node has a in(j) summation            
     * <b>Postconditions</b>:       None
     * 
     * @return  inJ a double containing a summation 
     */
    public double getIn()
    {
        return in;
    }
    
    /**
     * Stores the delta value
     * <b>Preconditions</b>:        The neural net has been created             
     * <b>Postconditions</b>:       None
     * 
     * @param  d  a double containing the delta value
     */
    public void setDelta(double d)
    {
        delta = d;
    }
    
    /**
     * Returns the delta value
     * <b>Preconditions</b>:        The neural net has been created, and the node has a delta value            
     * <b>Postconditions</b>:       None
     * 
     * @return  delta a double containing the delta value 
     */
    public double getDelta()
    {
        return delta;
    }

}
