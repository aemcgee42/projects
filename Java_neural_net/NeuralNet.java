import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Simulates a neural net
 * 
 * @author Austin E. McGee
 */
public class NeuralNet
{

    private ArrayList<NNode>[] net;   

    private int I;  
    private int O; 
    private int HL;         // Max number of hidden layers
    private int HLN;        
    private int hiddenLay; // Number of hidden layers
    private ArrayList<Integer> HLNperL;
    private double learn; 
    private double error; 
    private double scale;
    private ArrayList[] bins;
    private double globalError; // Holds the overall error for the validation
    private ArrayList<NeuralNet> arch;
    private int length; // Length of arch

    /**
     * Constructor for objects of class NeuralNet
     * <b>Preconditions</b>:        None                     
     * <b>Postconditions</b>:       A neural net has been created
     * 
     * @param  HL       an int containing the number of hidden layers
     * @param  HLN      an int containing the number of nodes in the hidden layers
     * @param  learn    a double containing the learning rate
     * @param  error    a double containing the error tolerance (use to determine the allowed time)
     */
    public NeuralNet(double learn, double error, int HL, int HLN)
    {
        //int I, int O, int HL, int HLN, double learn, double error
        this.learn = learn; 
        this.error = error;
        scale = 0;
        this.HL = HL;
        this.HLN = HLN;
        globalError = 0;
        arch = new ArrayList();
        length = 0;
    }

    /**
     * Constructor for objects of class NeuralNet
     * <b>Preconditions</b>:        None                     
     * <b>Postconditions</b>:       A neural net has been created
     * 
     * @param  HL       an int containing the number of hidden layers
     * @param  HLN      an int containing the number of nodes in the hidden layers
     * @param  learn    a double containing the learning rate
     * @param  error    a double containing the error tolerance (use to determine the allowed time)
     */
    public NeuralNet(NeuralNet n)
    {
        net = new ArrayList[n.net.length]; 
        net = n.net;
        I = n.I;  
        O = n.O; 
        HL = n.HL;         // Max number of hidden layers
        HLN = n.HLN;        
        //hiddenLay = new int[n.hiddenLay.length];    
        hiddenLay = n.hiddenLay; // Number of hidden layers
        HLNperL = new ArrayList(n.HLNperL); // Create a different amount of hidden layer nodes
        learn = n.learn; 
        error = n.error; 
        scale = n. scale;
        bins = n.bins;
        globalError = n.globalError; // Holds the overall error for the validation

    }

    /**
     * Builds a neural net
     * <b>Preconditions</b>:        A neural net has been created                      
     * <b>Postconditions</b>:       None
     * 
     * @param  I        an int containing the number of nodes in the input layer
     * @param  O        an int containing the number of nodes in the output layer
     * @param  HL       an int containing the number of hidden layers
     * @param  HLN      an int containing the number of nodes in the hidden layers
     */
    public void rebuild(int I, int O, int HL, int HLN)
    {

        this.I = I;  
        this.O = O; 
        this.HL = HL;  
        this.HLN = HLN;   
        //this.learn = learn; 
        //this.error = error;

        //O         O       O
        //O         O
        //O         O
        //O
        //O
        //O

        net = new ArrayList[HL+2];
        net[0] = new ArrayList();
        for (int i = 0; i < I; i++) // Input layer
        {    
            net[0].add(new NNode("I" + i, 1));
        }

        for(int k = 1; k < HL+1; k++)
        {
            net[k] = new ArrayList();
            for (int i = 0; i < HLN; i++)
            {
                net[k].add(new NNode("H" + i + k, 1));
            }
        }

        net[HL+1] = new ArrayList();
        for (int i = 0; i < O; i++)
        {
            net[HL+1].add(new NNode("O" + i, 1));
        }

        Random r = new Random();
        for (NNode node : net[0])
        {
            for (NNode node2 : net[1]) // first layer
            {
                double weight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
                node.setConnection(node2, weight);
                node2.setConnection(node, weight);
            }
        }

        for(int k = 1; k < HL+1; k++)
        {
            for (NNode node : net[k]) // first layer
            {
                for (NNode node2 : net[k+1])
                {
                    double weight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
                    node.setConnection(node2, weight);
                    node2.setConnection(node, weight);
                }
                double biasWeight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
                node.setBiasWeight(biasWeight);
            }
        }

        for (NNode node : net[HL+1])
        {
            double biasWeight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
            node.setBiasWeight(biasWeight);
        }

    }

    /**
     * Impleaments the back-propagation algorithm 
     * <b>Preconditions</b>:        The neural net has been created                    
     * <b>Postconditions</b>:       The neural net has learned
     * 
     * @param  name        a String containing the name of the input file
     * @param  hiddenNum   an int containing the number of hidden nodes
     * @param  hLayerNum   an int containing the number of hidden layers
     */
    public void backProp(String name, int hiddenNum, int hLayerNum)
    {

        ArrayList<Double> x = new ArrayList();
        ArrayList<Double> y = new ArrayList();
        scale = 0;

        try
        {
            FileIO example = new FileIO(name, FileIO.FOR_READING);            
            String line = example.readLine();
            scale = Double.parseDouble(line);
            line = example.readLine();
            String[] split = line.split(",");
            I = Integer.parseInt(split[0]);
            O = Integer.parseInt(split[1]);
            //x.add(Integer.parseInteger(split[0]));
            //y.add(Integer.parseInteger(split[1]));
            line = example.readLine();
            while(line != null)
            {

                split = line.split(",");
                for (int i = 0; i < I; i++)
                {
                    x.add(Double.parseDouble(split[i]));
                }

                for (int i = I; i < O + I; i++)
                {
                    y.add(Double.parseDouble(split[i]));
                }
                //y.add(Double.parseDouble(split[1]));
                line = example.readLine();
            }
        }

        catch (FileIOException fioe)
        {
            System.out.println(fioe.getMessage());
            return;
        }

        rebuild(I, O, hLayerNum, hiddenNum);

        long t= System.currentTimeMillis();
        double timeTemp = (60000*error); 
        long end = t + (long) timeTemp;

        while(System.currentTimeMillis() < end)
        {
            for (int q = 0; q < y.size(); q++)
            {
                System.out.println(net[HL+1].get(0).getActivation() * scale);
                double tempScale  = y.get(q) / scale;
                for (int i = 0; i < I; i++)
                {    
                    net[0].get(i).setActivation(x.get(I*q + i));
                }

                // 1st hidden layer to output
                for(int k = 1; k < HL+1; k++)
                {

                    for (NNode hNode : net[k])
                    {
                        double temp = 0;
                        //System.out.println(net[k].get(i).getBiasWeight());
                        Set<NNode> set =hNode.getNodes();
                        for (NNode node : set)
                        {
                            if (!net[k+1].contains(node))
                            {
                                temp += hNode.getWeight(node) * node.getActivation();
                            }

                        }
                        temp += hNode.getBiasWeight();
                        hNode.setActivation((1.0/(1.0+Math.exp(-temp))));
                        hNode.setIn(temp);
                    }
                }

                // Output layer foward feeding
                for (int j = 0; j < O; j++)
                {
                    double temp = 0;
                    Set<NNode> set = net[HL+1].get(j).getNodes();
                    for (NNode node : set)
                    {
                        temp += net[HL+1].get(j).getWeight(node) * node.getActivation();
                    }
                    temp += net[HL+1].get(j).getBiasWeight();
                    net[HL+1].get(j).setActivation((1.0/(1.0+Math.exp(-temp))));
                    net[HL+1].get(j).setIn(temp);
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                // Output back-propagate
                double delta = 0;
                for (int j = 0; j < O; j++)
                {
                    double temp = 0;
                    //Set<NNode> set = net[HL+1].get(j).getNodes();

                    //double gPrime = (Math.exp(-net[HL+1].get(j).getIn())) / Math.pow((1 + Math.exp(-net[HL+1].get(j).getIn())), 2);
                    double gPrime = (net[HL+1].get(j).getActivation() * (1.0 - net[HL+1].get(j).getActivation()));
                    delta = gPrime * (tempScale - net[HL+1].get(j).getActivation());
                    net[HL+1].get(j).setDelta(delta);
                }

                for (int i = HL; i > 0; i--)
                {
                    //for (int j = 0; j < HLN; j++)
                    for (NNode hNode : net[i])
                    {
                        double temp = 0;
                        Set<NNode> set = hNode.getNodes();
                        for (NNode node : set)
                        {
                            if (!net[i-1].contains(node))
                            {
                                temp += hNode.getWeight(node) * /*net[i+1].get(j)*/node.getDelta();
                            }

                        }
                        //temp += hNode.getBiasWeight();
                        double gPrime = hNode.getActivation() * (1 - hNode.getActivation());
                        double delta2 = gPrime * temp;
                        hNode.setDelta(delta2);
                    }
                }

              

                // Update weights
                for (int i = 0; i < HL+2; i++)
                {
                    for(NNode node : net[i])
                    {
                        Set<NNode> set = node.getNodes();
                        for (NNode con : set)
                        {
                            if (i == 0)
                            {
                                double temp = node.getWeight(con) + (learn * node.getActivation() * con.getDelta());
                                node.setConnection(con, temp);
                                con.setConnection(node, temp);
                            }

                            else if (!net[i-1].contains(con))
                            {
                                double temp = node.getWeight(con) + (learn * node.getActivation() * con.getDelta());
                                node.setConnection(con, temp);
                                con.setConnection(node, temp);
                            }
                        }

                        node.setBiasWeight((learn * 1 * node.getDelta() + node.getBiasWeight()));
                    }
                    //System.out.println("\r\ny(" + x.get(q) + "): " + net[HL+1].get(0).getActivation());
                }
            }
        }

    }

    /**
     * Impleaments the back-propagation algorithm for optimizing a neural net
     * <b>Preconditions</b>:        The neural net has been created                    
     * <b>Postconditions</b>:       The neural net has learned
     * 
     * @param  points        an ArrayList[] containing the points as a string
     * @param  points        an ArrayList containing the points to not include
     */
    public void backProp(ArrayList<String>[] points, ArrayList<String> skip)
    {
        ArrayList<Double> x = new ArrayList();
        ArrayList<Double> y = new ArrayList();
        //scale = 0;

        //FileIO example = new FileIO(name, FileIO.FOR_READING);            
        //String line = example.readLine();
        //scale = Double.parseDouble(line);
        //line = example.readLine();
        String[] split;
        //I = Integer.parseInt(split[0]);
        //O = Integer.parseInt(split[1]);
        //x.add(Integer.parseInteger(split[0]));
        //y.add(Integer.parseInteger(split[1]));
        //line = example.readLine();
        for (int j = 0; j < points.length; j++)
        {
            if(points[j] != skip)
            {
                for(String line : points[j])
                {

                    split = line.split(",");
                    for (int i = 0; i < I; i++)
                    {
                        x.add(Double.parseDouble(split[i]));
                    }

                    for (int i = I; i < O + I; i++)
                    {
                        y.add(Double.parseDouble(split[i]));
                    }
                    //y.add(Double.parseDouble(split[1]));
                    //line = example.readLine();
                }
            }
        }

        //rebuild(I, O, hLayerNum, hiddenNum);
        long t= System.currentTimeMillis();
        double timeTemp = (60000*error); 
        long end = t + (long) timeTemp;

        while(System.currentTimeMillis() < end)
        {
            for (int q = 0; q < y.size(); q++)
            {
                //System.out.println(net[hiddenLay+1].get(0).getActivation() * scale);
                double tempScale  = y.get(q) / scale;
                for (int i = 0; i < I; i++)
                {    
                    net[0].get(i).setActivation(x.get(I*q + i));
                }

                // 1st hidden layer to output
                for(int k = 1; k < hiddenLay+1; k++)    // hiddenLay is the number of hidden layers
                {

                    for (NNode hNode : net[k])
                    {
                        double temp = 0;
                        //System.out.println(net[k].get(i).getBiasWeight());
                        Set<NNode> set = hNode.getNodes();
                        for (NNode node : set)
                        {
                            if (!net[k+1].contains(node))
                            {
                                temp += hNode.getWeight(node) * node.getActivation();
                            }

                        }
                        temp += hNode.getBiasWeight();
                        hNode.setActivation((1.0/(1.0+Math.exp(-temp))));
                        hNode.setIn(temp);
                    }
                }

                // Output layer foward feeding
                for (int j = 0; j < O; j++)
                {
                    double temp = 0;
                    Set<NNode> set = net[hiddenLay+1].get(j).getNodes();
                    for (NNode node : set)
                    {
                        temp += net[hiddenLay+1].get(j).getWeight(node) * node.getActivation();
                    }
                    temp += net[hiddenLay+1].get(j).getBiasWeight();
                    net[hiddenLay+1].get(j).setActivation((1.0/(1.0+Math.exp(-temp))));
                    net[hiddenLay+1].get(j).setIn(temp);
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////
                // Output back-propagate
                double delta = 0;
                for (int j = 0; j < O; j++)
                {
                    double temp = 0;
                    //Set<NNode> set = net[HL+1].get(j).getNodes();

                    //double gPrime = (Math.exp(-net[HL+1].get(j).getIn())) / Math.pow((1 + Math.exp(-net[HL+1].get(j).getIn())), 2);
                    double gPrime = (net[hiddenLay+1].get(j).getActivation() * (1.0 - net[hiddenLay+1].get(j).getActivation()));
                    delta = gPrime * (tempScale - net[hiddenLay+1].get(j).getActivation()); // Get the delta for the output layer
                    net[hiddenLay+1].get(j).setDelta(delta);    // Update the delta
                }

                for (int i = hiddenLay; i > 0; i--)
                {
                    //for (int j = 0; j < HLN; j++)
                    for (NNode hNode : net[i])
                    {
                        double temp = 0;
                        Set<NNode> set = hNode.getNodes();
                        for (NNode node : set)
                        {
                            if (!net[i-1].contains(node))
                            {
                                temp += hNode.getWeight(node) * /*net[i+1].get(j)*/node.getDelta();
                            }

                        }
                        //temp += hNode.getBiasWeight();
                        double gPrime = hNode.getActivation() * (1 - hNode.getActivation());
                        double delta2 = gPrime * temp;
                        hNode.setDelta(delta2);
                    }
                }

            }

            // Update weights
            for (int i = 0; i < hiddenLay+2; i++)
            {
                for(NNode node : net[i])
                {
                    Set<NNode> set = node.getNodes();
                    for (NNode con : set)
                    {
                        if (i == 0)
                        {
                            double temp = node.getWeight(con) + (learn * node.getActivation() * con.getDelta());
                            node.setConnection(con, temp);
                            con.setConnection(node, temp);
                        }

                        else if (!net[i-1].contains(con))
                        {
                            double temp = node.getWeight(con) + (learn * node.getActivation() * con.getDelta());
                            node.setConnection(con, temp);
                            con.setConnection(node, temp);
                        }
                    }

                    node.setBiasWeight((learn * 1 * node.getDelta() + node.getBiasWeight()));
                }
                //System.out.println("\r\ny(" + x.get(q) + "): " + net[HL+1].get(0).getActivation());
            }
        }
    }
    
    
    /**
     * Rebuilds a neural net given the parameters
     * <b>Preconditions</b>:        The neural net has been rebuilt                
     * <b>Postconditions</b>:       None
     * 
     * @param  array    an arrayList containing the number of nodes in each hidden layer
     */
    public void rebuild2 (ArrayList<Integer> array)
    {
        net = new ArrayList[hiddenLay+2];
        //HLNperL = null;    // Store number of nodes per layer
        //HLNperL = new ArrayList();
        HLNperL = array;
        net[0] = new ArrayList();
        for (int i = 0; i < I; i++) // Input layer
        {    
            net[0].add(new NNode("I" + i, 1));
        }

        // Create all hidden nodes
        for(int k = 1; k < hiddenLay+1; k++)
        {
            net[k] = new ArrayList();
            for (int i = 0; i < (array.get(k-1)); i++)  // hn contains the number nodes for each layer
            {
                net[k].add(new NNode("H" + i + k, 1));
            }
        }

        net[hiddenLay+1] = new ArrayList();   // Create output layer
        for (int i = 0; i < O; i++)
        {
            net[hiddenLay+1].add(new NNode("O" + i, 1));  // Create output node
        }

        Random r = new Random();
        for (NNode node : net[0])
        {
            for (NNode node2 : net[1]) // first layer
            {
                double weight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
                node.setConnection(node2, weight);
                node2.setConnection(node, weight);
            }
        }

        for(int k = 1; k < hiddenLay+1; k++)
        {
            for (NNode node : net[k]) // first layer
            {
                for (NNode node2 : net[k+1])
                {
                    double weight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
                    node.setConnection(node2, weight);
                    node2.setConnection(node, weight);
                }
                double biasWeight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
                node.setBiasWeight(biasWeight);
            }
        }

        for (NNode node : net[hiddenLay+1])
        {
            double biasWeight = -0.1 + (0.1 - -0.1) * r.nextDouble();   // -0.1 to 0.1
            node.setBiasWeight(biasWeight);
        }
    }

    /**
     * Impleaments the feed foward algorithm for optimizing a neural net
     * <b>Preconditions</b>:        The neural net has learned                  
     * <b>Postconditions</b>:       None
     * 
     * @param  array    an arrayList containing the number of nodes in each hidden layer
     * @param  lvl      an int containing the number of hidden layer to consider at a particular time
     */
    private void feedOptimal(ArrayList<Integer> array, int lvl)
    {
        
        if (lvl == 0)
        {
            globalError = 0;
            //do work with a completely filled out array

            

            for (ArrayList<String> b : bins)
            {
                // Pass in all bins along with the bin to skip over
                rebuild2(array);
                backProp(bins, b);
                double error = 0;

                for (int i = 0; i < bins.length; i++)
                {
                    if (bins[i] == b)
                    {
                        for (ArrayList<String> points : bins)
                        {
                            error = test(points);
                        }
                    }
                }

                //error /= bins.length-1;
                globalError += error;

            }
            globalError /= bins.length;
            //hiddenLay = lvl;    // Store total hidden nodes
            NeuralNet temp = new NeuralNet(this);
            //temp = this;
            arch.add(temp);    // Store the architecture
            length++;
            // Print the current architecture
            System.out.println("\r\nArchitecture: ");
            System.out.println("Inputs: " + I);
            System.out.println("Outputs: " + O);
            System.out.println("Hidden Layers: " + hiddenLay);
            for(int i = 0; i < hiddenLay; i++)
            {
                System.out.println("Number of hidden nodes for layer " + (i+1) + ": " + array.get(i));
            }
            System.out.println("Average error: " + globalError);

            return;
        }

        for (int i = 1; i < HLN+1; i++)  
        {
            //int index =;
            array.set(lvl-1, i);   // Set number of nodes
            feedOptimal(array, lvl-1);

        }
    }

    /**
     * Finds the best architecture for a neural net
     * <b>Preconditions</b>:        None                
     * <b>Postconditions</b>:       None
     * 
     * @param  bins     an ArrayList[] containing the the points
     * @param  hiddenN  an int containing the max number of hidden layer nodes in each layer
     * @param  hiddenL  an int containing the max number of hidden layers
     * @param  fileName a String containing th ename of the input file
     * @return  arch    an ArrayList containing the various architectures for the neural net
     * 
     */
    public ArrayList<NeuralNet> identifyBest(ArrayList[] bins, int hiddenN, int hiddenL, String fileName)
    {

        ArrayList<Double> x = new ArrayList();
        ArrayList<Double> y = new ArrayList();

        scale = 0;

        try
        {
            FileIO example = new FileIO(fileName, FileIO.FOR_READING);            
            String line = example.readLine();
            scale = Double.parseDouble(line);
            line = example.readLine();
            String[] split = line.split(",");
            I = Integer.parseInt(split[0]);
            O = Integer.parseInt(split[1]);
        }

        catch (FileIOException fioe)
        {
            System.out.println(fioe.getMessage());
            return null;
        }

        ArrayList<Integer> hn = new ArrayList();    // number of nodes in each hidden layer
        for (int i = 0; i < HL; i++)   // Populate array of hidden nodes
        {
            hn.add(0);
        }
        //hn[0] = 2;
        //hn[1] = 3;
        //HLNperL = hn;
        this.bins = bins;
        for (int i = 0; i < hiddenL+1; i++)
        {
            hiddenLay = i;    // Set current number of hidden layers
            feedOptimal(hn, i);
        }
        return arch;
    }

    /**
     * Computes the neural net using foward feeding
     * <b>Preconditions</b>:        The neural net has learned                  
     * <b>Postconditions</b>:       None
     * 
     * @param  x        a double[] containing the inputs
     * @param  y        a double[] containing the outputs
     * @return  net[HL+1].get(0).getActivation() * scale    a double containing the neural net output
     * 
     */
    public double display(double[] x, double[] y)
    {
        for (int q = 0; q < y.length; q++)
        {
            //System.out.println(net[HL+1].get(0).getActivation() * scale);
            double tempScale  = y[q] / scale;
            for (int i = 0; i < I; i++)
            {    
                net[0].get(i).setActivation(x[I*q + i]);
            }

            // 1st hidden layer to output
            for(int k = 1; k < HL+1; k++)
            {

                for (NNode hNode : net[k])
                {
                    double temp = 0;
                    //System.out.println(net[k].get(i).getBiasWeight());
                    Set<NNode> set = hNode.getNodes();
                    for (NNode node : set)
                    {
                        if (!net[k+1].contains(node))
                        {
                            temp += hNode.getWeight(node) * node.getActivation();
                        }

                    }
                    temp += hNode.getBiasWeight();
                    hNode.setActivation((1.0/(1.0+Math.exp(-temp))));
                    hNode.setIn(temp);
                }
            }

            // Output layer foward feeding
            for (int j = 0; j < O; j++)
            {
                double temp = 0;
                Set<NNode> set = net[HL+1].get(j).getNodes();
                for (NNode node : set)
                {
                    temp += net[HL+1].get(j).getWeight(node) * node.getActivation();
                }
                temp += net[HL+1].get(j).getBiasWeight();
                net[HL+1].get(j).setActivation((1.0/(1.0+Math.exp(-temp))));
                net[HL+1].get(j).setIn(temp);
            }
        }
        double totError = 0;
        for (int j = 0; j < y.length; j++)
        {
            for (int i = 0; i < x.length; i++)
            {
                System.out.println("Input: " + x[i]);
            }

            System.out.println("Measured Output: " +  y[j]);
            //Get average output
            //double measured = net[HL+1].get(0).getActivation() * scale;
            //totError += Math.abs((measured - y[j]));
            System.out.println("Neural Net Output: " +  net[HL+1].get(0).getActivation() * scale + "\r\n");
        }
        //HL = 2;

        //totError /= y.length;
        //System.out.println("Average Error: " +  totError + "\r\n");    
        return net[HL+1].get(0).getActivation() * scale;

    }

    /**
     * Computes the neural net using foward feeding for testing if it is optimal
     * <b>Preconditions</b>:        The neural net has learned                  
     * <b>Postconditions</b>:       None
     * 
     * @param  x        an ArrayList containing the inputs
     * @param  y        an ArrayList containing the outputs
     * @return  net[hiddenLay+1].get(0).getActivation() * scale    a double containing the average error
     * 
     */
    public double test(ArrayList<String> points)
    {
        // Convert strings to doubles
        String[] split;
        ArrayList<Double> x = new ArrayList();
        ArrayList<Double> y = new ArrayList();
        double totError = 0;    // Holds total error

        for(String line : points)
        {

            split = line.split(",");
            for (int i = 0; i < I; i++)
            {
                x.add(Double.parseDouble(split[i]));
            }

            for (int i = I; i < O + I; i++)
            {
                y.add(Double.parseDouble(split[i]));
            }
            //y.add(Double.parseDouble(split[1]));
            //line = example.readLine();
        }

        for (int q = 0; q < y.size(); q++)
        {
            //System.out.println(net[hiddenLay+1].get(0).getActivation() * scale);    // Test
            double tempScale  = y.get(q) / scale;
            for (int i = 0; i < I; i++)
            {    
                net[0].get(i).setActivation(x.get(I*q + i));
            }

            // 1st hidden layer to output
            for(int k = 1; k < hiddenLay+1; k++)
            {

                for (NNode hNode : net[k])
                {
                    double temp = 0;
                    //System.out.println(net[k].get(i).getBiasWeight());
                    Set<NNode> set = hNode.getNodes();
                    for (NNode node : set)
                    {
                        if (!net[k+1].contains(node))   // Check if the connection is not in front of the node
                        {
                            temp += hNode.getWeight(node) * node.getActivation();
                        }

                    }
                    temp += hNode.getBiasWeight();
                    hNode.setActivation((1.0/(1.0+Math.exp(-temp))));
                    hNode.setIn(temp);
                }
            }

            // Output layer foward feeding
            for (int j = 0; j < O; j++)
            {
                double temp = 0;
                Set<NNode> set = net[hiddenLay+1].get(j).getNodes();    // Easily loop through all of the nodes
                for (NNode node : set)
                {
                    temp += net[hiddenLay+1].get(j).getWeight(node) * node.getActivation();
                }
                temp += net[hiddenLay+1].get(j).getBiasWeight();
                net[hiddenLay+1].get(j).setActivation((1.0/(1.0+Math.exp(-temp))));
                net[hiddenLay+1].get(j).setIn(temp);
                
                
            }
            totError += Math.abs(net[hiddenLay+1].get(0).getActivation() * scale - y.get(q));
        }
        // Return the output for the neural net
        //System.out.println("Average Error: " +  net[hiddenLay+1].get(0).getActivation() * scale + "\r\n");  // Display the average error for entire validation    
        return totError / y.size();

    }

    /**
     * Returns the overall error for the validation phase
     * <b>Preconditions</b>:        The neural net has had cross-fold validation performed on it                 
     * <b>Postconditions</b>:       None
     * 
     * @return  globalError a double containing the overall error for the validation phase
     */
    public double getGlobEr()
    {
        return globalError;
    }
    
    /**
     * Returns the number of nodes for each layer
     * <b>Preconditions</b>:        The neural net has had cross-fold validation performed on it                 
     * <b>Postconditions</b>:       None
     * 
     * @return  HLNperL an ArrayList<Integer> containing the overall error for the validation phase
     */
    public ArrayList<Integer> getNodes()
    {
        return HLNperL;
    }

    /**
     * Displays the neural net as a string
     * <b>Preconditions</b>:        The neural net has had cross-fold validation performed on it                 
     * <b>Postconditions</b>:       None
     * 
     */
    public void string()
    {
        System.out.println("\r\nArchitecture: ");
        System.out.println("Inputs: " + I);
        System.out.println("Outputs: " + O);
        System.out.println("Hidden Layers: " + hiddenLay);  // Display hidden layers number
        for(int i = 0; i < hiddenLay; i++)  // Loop through all hidden layers
        {
            System.out.println("Number of hidden nodes for layer " + (i+1) + ": " + HLNperL.get(i));
        }
        System.out.println("Average error: " + globalError);
    }
}
