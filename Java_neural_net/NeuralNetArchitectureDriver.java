import java.util.*;
/**
 * Optimizes a neural net
 * 
 * @author Austin E. McGee 
 * 
 * The optimal architecture was found to be 2 hidden layers with 7 nodes in first layer and 2 node in the second.
 * As the complexity increased, the average error converged on a certain error value, allowing this architecture to
 * be choosen.
 * 
 */
public class NeuralNetArchitectureDriver
{

    public static void main(String args[])
    {
        String fileName;
        int input = 0;
        int output = 0;
        int hiddenL = 0;
        int hiddenN = 0;
        double learn = 0;
        double error = 0;
        int crossValBins = 0;

        int[] wubs = new int[3];

        try
        {
            FileIO file = new FileIO(args[0], FileIO.FOR_READING);            
            String line = file.readLine();
            fileName= line;
            line = file.readLine();
            crossValBins = Integer.parseInt(line);

            line = file.readLine();
            hiddenL = Integer.parseInt(line);
            line = file.readLine();
            hiddenN = Integer.parseInt(line);
            line = file.readLine();
            learn = Double.parseDouble(line);
            line = file.readLine();
            error = Double.parseDouble(line);

        }

        catch (FileIOException fioe)
        {
            System.out.println(fioe.getMessage());
            return;
        }

        //ArrayList<Double> x = new ArrayList();
        ArrayList<String> points = new ArrayList();
        ArrayList<Double>[] x;
        ArrayList<Double>[] y;
        double totError = 0;
        int numPoints = 0;

        ArrayList<String>[] kbin = new ArrayList[crossValBins];
        for (int i = 0; i < crossValBins; i++)
        {
            kbin[i] = new ArrayList();
        }

        try
        {
            FileIO example = new FileIO(fileName, FileIO.FOR_READING);            
            String line = example.readLine();
            //scale = Double.parseDouble(line);
            line = example.readLine();
            String[] split = line.split(",");
            input = Integer.parseInt(split[0]);
            output = Integer.parseInt(split[1]);
            line = example.readLine();
            while(line != null)
            {
                points.add(line);          
                numPoints++;
                line = example.readLine();
            }
        }

        catch (FileIOException fioe)
        {
            System.out.println(fioe.getMessage());
            return;
        }

        x = new ArrayList[numPoints];
        for (ArrayList<Double> list : x)
        {
            list = new ArrayList();
        }
        y = new ArrayList[numPoints];
        for (ArrayList<Double> list : y)
        {
            list = new ArrayList();
        }

        try
        {
            FileIO example = new FileIO(fileName, FileIO.FOR_READING);            
            String line = example.readLine();
            //scale = Double.parseDouble(line);
            line = example.readLine();
            String[] split = line.split(",");
            input = Integer.parseInt(split[0]);
            output = Integer.parseInt(split[1]);
            //x.add(Integer.parseInteger(split[0]));
            //y.add(Integer.parseInteger(split[1]));
            //x = new double[input];
            //y = new double[output];
            line = example.readLine();
            int count = 0;

        }

        catch (FileIOException fioe)
        {
            System.out.println(fioe.getMessage());
            return;
        }
        //random.nextInt(max - min + 1) + min
        Random r = new Random();
        for (int i = 0; i < points.size(); i++)
        {
            for(int j = 0; j < crossValBins; j++)
            {
                int index = r.nextInt((points.size()-1) - 0 + 1) + 0;
                kbin[j].add(points.remove(index));                
            }            
        }
        NeuralNet net = new NeuralNet(learn, error, hiddenL, hiddenN);
        //net.backProp(wubs, 3);
        ArrayList<NeuralNet> sol = net.identifyBest(kbin, hiddenN, hiddenL, fileName);
        //net.backProp(fileName, hiddenN, hiddenL);

        // Break up structure to easily get the neural nets
        NeuralNet smallest = sol.get(0);
        int sum = 0;    // Store number of nodes total

        for(int i : smallest.getNodes())
        {
            sum += i;
        }

        for (int i = 1; i < sol.size(); i++)
        {
            int sum2 = 0;
            for(int j : sol.get(i).getNodes())
            {
                sum2 += j;
            }
            if(Math.abs(sol.get(i).getGlobEr() - smallest.getGlobEr()) >= 1 || sum2 < sum)    // Check tolerance
            {
                if (sol.get(i).getGlobEr() < smallest.getGlobEr())  // Check if smaller
                {
                    smallest = sol.get(i);
                    sum = sum2;
                }
            }
            //int wubs = 0;
        }
        // Print optimal architecture
        System.out.println("\r\nOptimal architecture: ");
        smallest.string();  // Display neural net
    }
}