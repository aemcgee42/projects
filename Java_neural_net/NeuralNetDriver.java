

import java.util.*;

/**
 * Trains a neural net
 * 
 * @author Austin E. McGee
 */
public class NeuralNetDriver
{

    public static void main(String args[])
    {
        String fileName;
        int input = 0;
        int output = 0;
        int hiddenL = 0;
        int hiddenN = 0;
        double learn = 0;
        double error = 0;

        
        
        try
        {
            FileIO file = new FileIO(args[0], FileIO.FOR_READING);            
            String line = file.readLine();
            fileName= line;

            line = file.readLine();
            hiddenL = Integer.parseInt(line);
            line = file.readLine();
            hiddenN = Integer.parseInt(line);
            line = file.readLine();
            learn = Double.parseDouble(line);
            line = file.readLine();
            error = Double.parseDouble(line);

        }

        catch (FileIOException | ArrayIndexOutOfBoundsException fioaioobe)
        {
            System.out.println(fioaioobe.getMessage());
            return;
        }

        //ArrayList<Double> x = new ArrayList();
        //ArrayList<Double> y = new ArrayList();
        double[] x;
        double[] y;
        double totError = 0;
        int numPoints = 0;

        NeuralNet net = new NeuralNet(learn, error, hiddenL, hiddenN);
        
        net.backProp(fileName, hiddenN, hiddenL);
        try
        {
            FileIO example = new FileIO(fileName, FileIO.FOR_READING);            
            String line = example.readLine();
            //scale = Double.parseDouble(line);
            line = example.readLine();
            String[] split = line.split(",");
            input = Integer.parseInt(split[0]);
            output = Integer.parseInt(split[1]);
            //x.add(Integer.parseInteger(split[0]));
            //y.add(Integer.parseInteger(split[1]));
            x = new double[input];
            y = new double[output];
            
            line = example.readLine();
            while(line != null)
            {

                split = line.split(",");
                for (int i = 0; i < input; i++)
                {
                    x[i] = (Double.parseDouble(split[i])) ;
                }

                for (int i = 0; i < output; i++)
                {
                    y[i] = (Double.parseDouble(split[input+i]));
                }
                totError += (Math.abs(net.display(x, y) - y[0]));
                numPoints++;
                line = example.readLine();
            }
        }

        catch (FileIOException fioe)
        {
            System.out.println(fioe.getMessage());
            return;
        }

        //double totError = 0;
        //double measured = net[HL+1].get(0).getActivation() * scale;
        //totError = Math.abs((totError - y[j]));
        System.out.println("Average Error: " +  totError/numPoints + "\r\n");
        int wubs = 0;
    }
}
