import java.util.*;
/**
 * Driver for The graph and its operations
 * 
 * @author Austin E. McGee
 */
public class Driver
{
    public static void main(String[] args)
    {


        try
        {
            Graph graph = new Graph(args[0], args[1]);
           

            
            graph.genAlg(Integer.parseInt(args[2]), Integer.parseInt(args[3]));
 
        }

        catch(ArrayIndexOutOfBoundsException | NumberFormatException aioobnfe)
        {
            System.out.println("\r\nPlease enter both filenames (one for the vertices and one for the edges)");
            System.out.println("from the command line. Make sure they are the proper format for reading\r\n");
            System.out.println("Also include the size of the population and the number of generations.\r\n");
            return;
        }
        catch (FileIOException fioe)
        {
            System.out.println(fioe.getMessage());
            return;
        }

        catch (GraphException ge)
        {
            System.out.println(ge.getMessage());
            return;
        }

        
        
    }
}