
/**
 * Contains a node with a given wieght
 * This is used by the adjancey list to store what node points to what
 * 
 * @author Austin E. McGee 
 */
public class Edge<T>
{
    T nodeID1; // Contains the label for the start node
    T nodeID2; // Contains the label for the end node
    double weight;  // Contains the weight for the edge

    /**
     * Constructor for objects of class Node
     * <b>Preconditions</b>:        The graph must have been created and contain vertices
     * <b>Postconditions</b>:       The node has been set
     * 
     * @param  node     a T containing the label for the node
     * @param  weight   a double containing the weight for the edge
     * 
     */
    public Edge(T nodeID1, T nodeID2, double weight)
    {
        this.nodeID1 = nodeID1;
        this.nodeID2 = nodeID2;
        this.weight = weight;
    }

    /**
     * Returns the start node's label
     * <b>Preconditions</b>:        The node must have been created and defined
     * <b>Postconditions</b>:       The node's label may now be used
     * 
     * @returns node    a T containing the node's label
     * 
     */
    public T getNode1()
    {
        return nodeID1;
    }

    /**
     * Returns the end node's label
     * <b>Preconditions</b>:        The node must have been created and defined
     * <b>Postconditions</b>:       The node's label may now be used
     * 
     * @returns node    a T containing the node's label
     * 
     */
    public T getNode2()
    {
        return nodeID2;
    }

    /**
     * Returns the node's weight
     * <b>Preconditions</b>:        The node must have been created and defined
     * <b>Postconditions</b>:       The node's weight may now be used
     * 
     * @returns weight    a double containing the node's weight
     * 
     */
    public double getWeight()
    {
        return weight;
    }
}
