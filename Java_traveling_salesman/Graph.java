import java.util.ArrayList;
//import java.util.ArrayStack;
import java.util.*;

/**
 * Represents a Graph data structure
 * 
 * @author Austin McGee
 * 
 */
public class Graph
{

    private HashMap <Vertex<String>,Integer> reference;  // Holds the String keys for the vertices and their number identifiers
    private HashMap <Integer,Vertex<String>> backReference;   // Holds number identifiers for the String keys of the vertices
    private HashMap<String, Vertex<String>> keyToVert; // Converts the key to a vertex

    private double[][] matrix;     // Holds the adjacency matrix
    private int vertexNum = 0;  // Holds the number of vertices
    private int edgeNum = 0;  // Holds the number of edges

    
    /**
     * Constructor for objects of class Graph for the adjacency matrix and adjacency list
     * <b>Preconditions</b>:        None
     * <b>Postconditions</b>:       The graph has been created
     * 
     * @param vertecies a String containing the name of the file of vertecies
     * @param edges     a String containing the name of the file of edges
     * @throws FileIOException  if there is a problem reading or writing to the file
     * @throws GraphException   if there is a problem adding the vertices or edges
     */
    public Graph(String vertecies, String edges) throws FileIOException, GraphException
    {

        reference = new HashMap<Vertex<String>, Integer>();
        backReference = new HashMap<Integer,Vertex<String>>();
        keyToVert = new HashMap<String, Vertex<String>>();

        try{
            FileIO vertexFile = new FileIO(vertecies, FileIO.FOR_READING);
            FileIO edgeFile = new FileIO(edges, FileIO.FOR_READING);
            String line = vertexFile.readLine();
            while(line != null)
            {
                addVertex(line);
                line = vertexFile.readLine();
            }

            createMatrix(); // Creates the matrix

            line = edgeFile.readLine();
            while(line != null)
            {
                String[] split = line.split(" ");
                addEdge(split[0], split[1], (Double.parseDouble(split[2])));
                line = edgeFile.readLine();
            }
        }
        catch (FileIOException fioe)
        {
            throw new FileIOException();
        }
        
        catch (GraphException ge)
        {
            throw new GraphException();
        }

    }

    /**
     * Creates an adjacency matrix
     * <b>Preconditions</b>:        The number of vertices hve been determined
     * <b>Postconditions</b>:       The matrix has been created
     * 
     */
    private void createMatrix()
    {
        matrix = new double[vertexNum][vertexNum];
    }

    /**
     * Adds a vertex to the graph
     * <b>Preconditions</b>:        The graph must be created                       
     * <b>Postconditions</b>:       A vertex has been added
     * 
     * @param  item             a String containing the search key for the vertices
     * @throws GraphException   if no new vertices could be added
     */
    void addVertex(String item) throws GraphException
    {
        try
        {
            addVertexMatrix(item);
        }

        catch (GraphException ge)
        {
            throw new GraphException();
        }
    }

    /**
     * Adds a vertex to the matrix
     * <b>Preconditions</b>:        The graph must be created                       
     * <b>Postconditions</b>:       A vertex has been added
     * 
     * @param  item             a String containing the search key for the vertices
     * @throws GraphException   if no new vertices could be added
     */
    public void addVertexMatrix(String item) throws GraphException
    {

        Vertex<String> vertex = new Vertex<String>(item);
        reference.put(vertex, vertexNum);
        backReference.put(vertexNum, vertex);
        keyToVert.put(item, vertex);

        vertexNum++;
 
    }

    /**
     * Adds an edge to the graph
     * <b>Preconditions</b>:        The graph and its vertices must be created                       
     * <b>Postconditions</b>:       An edge has been added
     * 
     * @param  start_vertex_key a String containing the key for the first vertex
     * @param  end_vertex_key   a String containing the key for the second vertex
     * @param  edge_weight      a double containing the weight for the edge
     * @throws GraphException   if the vertices do not exist
     */
    void addEdge(String start_vertex_key, String end_vertex_key, double edge_weight) throws GraphException
    {
        try
        {
            addEdgeMatrix(start_vertex_key, end_vertex_key, edge_weight);
        }

        catch (GraphException ge) 
        {
            throw new GraphException("\r\nThe given vertices do not exists\r\n");
        }
    }

    /**
     * Adds an edge to the matrix
     * <b>Preconditions</b>:        The graph must be created                       
     * <b>Postconditions</b>:       An edge has been added
     * 
     * @param  start_vertex_key a String containing the key for the first vertex
     * @param  end_vertex_key   a String containing the key for the second vertex
     * @param  edge_weight      a double containing the weight for the edge
     * @throws GraphException   if the vertices do not exist
     */
    void addEdgeMatrix(String start_vertex_key, String end_vertex_key, double edge_weight) throws GraphException
    {
        int start = 0;
        int end = 0;
        try 
        {
            if(keyToVert.get(start_vertex_key) != null && keyToVert.get(end_vertex_key) != null)
            {
                start = reference.get(keyToVert.get(start_vertex_key)); // Get the vertex and pass in
                end = reference.get(keyToVert.get(end_vertex_key));     // the String key to get the position
            }                                                           // in the matrix
            else
            {
                throw new ArrayIndexOutOfBoundsException();
            }
            matrix[start][end] = edge_weight;
        }
        catch (ArrayIndexOutOfBoundsException aioobe) 
        {
            throw new GraphException("\r\nThe given vertices do not exists\r\n");
        }
    }

    /**
     * Genetic algorithm for the traveling salesman problem
     * <b>Preconditions</b>:        The graph, vertices, and edges must be created and defined         
     * <b>Postconditions</b>:       A solution has been found
     * 
     * @param  popSize              an int containing the population size
     * @param  genNum               an int containing the number of generations
     * 
     * @throws  GraphException        if the population size is too small
     */
    public void genAlg(int popSize, int genNum) throws GraphException
    { 
        ArrayList newGen = new ArrayList();
        ArrayList pop = new ArrayList();
        for (int j = 0; j < popSize; j++)
        {
            ArrayList list = new ArrayList();
            for (int i = 1; i < vertexNum; i++)
            {
                list.add(backReference.get(i));
            }
            // Randomly reorder the tour
            Collections.shuffle(list);
            double cost = calCost(list);
            Tour tour = new Tour(list, cost);   // create tour
            pop.add(tour);
        }

        try
        {
            System.out.println("Genneration number: " + 1);
            newGen = new ArrayList(nextGen(pop));
            for (int i = 0; i < genNum-1; i++)
            {
                System.out.println("Genneration number: " + (i+1));
                newGen = new ArrayList(nextGen(newGen));
            }
        }

        catch(GraphException ge)
        {
            throw new GraphException("\r\nPopulation too small.\r\n");
        }

        display((Tour) newGen.get(0));
    }

    /**
     * Creates the next generation
     * <b>Preconditions</b>:        A population has been created       
     * <b>Postconditions</b>:       A new generataion has been created
     * 
     * @param  pop               an ArrayList containing a population
     * 
     * @returns newGen                an ArrayList containing the next generation
     * @throws  GraphException        if the population size is too small
     */
    public ArrayList nextGen(ArrayList pop) throws GraphException
    { 
        Collections.sort(pop); // sort
        ArrayList tempPop = new ArrayList(pop); // For making modifications
        ArrayList newGen = new ArrayList();
        int popIndex = 0;   // The index to get 2 random tours
        if (pop.size() >= 10)
        {
            for (int i = 0; i < 10; i++)
            {
                newGen.add(((Tour) tempPop.get((tempPop.size() - 1))));   // Save 10 best
                tempPop.remove((tempPop.size() - 1));  // Prevent duplicates when selecting random cities
            }
        }

        else 
        {
            throw new GraphException("\r\nPopulation too small.\r\n");
        }

        while(newGen.size() != pop.size())
        {
            Random r = new Random();
            popIndex = r.nextInt(((pop.size() - 1) - 0) + 1) + 0;   // 0-499 if pop size is 500
            ArrayList temp = new ArrayList();   // to hold set for crossover

            for (int i = 0; i < 10; i++)
            { 
                // select random members of the current population
                Tour t = new Tour (((Tour) (pop.get(popIndex))).getTour(), ((Tour) pop.get(popIndex)).getTotCost() * -1);
                temp.add(t);
                popIndex = r.nextInt(((pop.size() - 1) - 0) + 1) + 0;   // 0-499 if pop size is 500
            }
            Collections.sort(temp);
            popIndex = r.nextInt(((temp.size() - 1)  - (temp.size() - 4)) + 1) + (temp.size() - 4);   // best 4
            if(((((Tour) temp.get(popIndex)).getTotCost() * -1) < 7810))
            {
                int z = 0;
            }
            ArrayList cross1 = new ArrayList(((Tour) temp.get(popIndex)).getTour());    // Get the tour's city list from temp
            temp.remove(popIndex);
            popIndex = r.nextInt(((temp.size() - 1)  - (temp.size() - 3)) + 1) + (temp.size() - 3);   // best 3
            ArrayList cross2 = new ArrayList(((Tour) temp.get(popIndex)).getTour());
            crossOver(cross1, cross2, newGen);
        }
        return newGen;
    }

    /**
     * Calculates the cost of the tour
     * <b>Preconditions</b>:        A tour has been created         
     * <b>Postconditions</b>:       The cost has been generated
     * 
     * @param  list              a List containing the tour
     * 
     * @returns cost             a double containing the total cost
     */
    public double calCost(List list) 
    { 
        double cost = 0;
        int v0 = 0;// Get starting city
        Vertex v1;
        Vertex v2 = ((Vertex) list.get(0)); // Get next city
        Vertex<String> vr1; 
        Vertex<String> vr2 = keyToVert.get(v2.getKey()); // Get the actual vertices from the hashmap
        cost += matrix[v0][reference.get(vr2)]; // Compute the cost of the two vertices
        for (int i = 0; i < (list.size()-1); i++)
        {
            v1 = ((Vertex) list.get(i));
            v2 = ((Vertex) list.get(i+1));  // Get to vertices
            vr1 = keyToVert.get(v1.getKey());
            vr2 = keyToVert.get(v2.getKey()); // Get the actual vertices from the hashmap
            cost += matrix[reference.get(vr1)][reference.get(vr2)]; // Compute the cost of the two vertices
            // which make up an edge
        }

        v1 = ((Vertex) list.get(list.size()-1));  // Get last city  
        v0 = 0; // Get ending city
        vr1 = keyToVert.get(v1.getKey());
        cost += matrix[reference.get(vr1)][v0]; // Compute the cost of the two vertices
        // which make up an edge

        return cost; //* -1;
    }

    /**
     * Mixes up two chromosomes
     * <b>Preconditions</b>:        Two tours have been successfully created         
     * <b>Postconditions</b>:       The two chromosomes have been mixed
     * 
     * @param  cross1     an ArrayList containing a tour
     * @param  cross2     an ArrayList containing another tour
     * @param  pop        an ArrayList containing the next generation
     * 
     */
    private void crossOver(ArrayList cross1, ArrayList cross2, ArrayList newGen) 
    {
        Random r = new Random();
        int ind1 = r.nextInt(((vertexNum-2) - 0) + 1) + 0;   // 0-13 if pop size is 15
        int ind2 = r.nextInt(((vertexNum-2) - ind1) + 1) + ind1;   // ind1 - 13 if pop size is 15

        // store crossover values
        for (int i = ind1; i < (ind2+1); i++)
        {
            Vertex newTemp = (Vertex) cross1.get(i);
            cross1.set(i, cross2.get(i));
            cross2.set(i, newTemp);
        }

        cross1 = fixCrossOver(cross1);  // Fix the duplicates generated
        double cos = calCost(cross1);   // Calculate cost
        Tour newTour = new Tour(cross1, cos);   // 
        newGen.add(newTour);

        cross2 = fixCrossOver(cross2);
        cos = calCost(cross2);
        newTour = new Tour(cross2, cos);
        newGen.add(newTour);
    }

    /**
     * Removes the duplicates from the crossover attempt
     * <b>Preconditions</b>:        A crossover attempt has been started on a given tour         
     * <b>Postconditions</b>:       The crossover process has completed
     * 
     * @param  cross2     an ArrayList containing a tour
     * @return cross2     an ArrayList containing a new tour
     */
    private ArrayList fixCrossOver(ArrayList cross2) 
    {
        ArrayList unrepresented = new ArrayList();  // delete from arraylist when a city is visted the first time
        int[][] represented = new int[3][vertexNum-1];    // row 1 = the cities
        for (int i = 0; i < vertexNum-1; i++)                   // row 2 = first appearance in tour
        {                                                       // row 3 = second appearance in tour
            represented[0][i] = i+1;
            unrepresented.add(i+1);
        }

        for(int i = 0; i < vertexNum-1; i++)
        {
            Vertex v = (Vertex) cross2.get(i);
            int index = reference.get(v);
            if (unrepresented.indexOf(index) != -1)
            {
                unrepresented.remove(unrepresented.indexOf(index));
                represented[1][index-1] = i;  // store position
            }

            else
            {
                represented[2][index-1] = i;  // store position
            }
        }

        for (int i = 0; i < cross2.size(); i++)
        {
            if(represented[2][i] > 0)
            {
                Random r = new Random();
                int ind1 = r.nextInt((2 - 1) + 1) + 1;   // 1-2 for the two positions
                int pos = represented[ind1][i];
                int ind2 = r.nextInt(((unrepresented.size() - 1) - 0) + 1) + 0;   // to get a random city
                int newCity = (Integer) unrepresented.get(ind2);

                cross2.set(pos, backReference.get(newCity));
                unrepresented.remove(ind2);
            }

        }

        return cross2;
    }

    /**
     * Displays the tour
     * <b>Preconditions</b>:        A solution has been found    
     * <b>Postconditions</b>:       Both the vertex numbers and the cost have been displayed
     * 
     * @param  sol     a Tour containing the solution
     * 
     */
    private void display(Tour sol) 
    {
        ArrayList tour = sol.getTour();

        for (int i = 0; i < tour.size(); i++)
        {
            tour.set(i, reference.get(tour.get(i)));
        }

        System.out.println(sol.getTotCost() * -1);
        System.out.println(tour);
    }

}

