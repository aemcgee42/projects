
/**
 * Exception for Graph
 * 
 * @author Austin E. McGee
 */
public class GraphException extends Exception
{
    /**
     * Creates the exception
     * <b>Preconditions</b>:    The graph has too many vertices being added
     * <b>Postconditions</b>:   Displays an error
     * 
     */
   public GraphException()
   {
      this("\r\nCould not add anymore vertecices");
   }

    /**
     * Creates the exception
     * <b>Preconditions</b>:    The graph has too many vertices being added
     * <b>Postconditions</b>:   Displays an error
     * 
     * @param   s               a String containing the error message
     */
   public GraphException(String s)
   {
      super(s);
   }
}