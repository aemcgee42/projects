import java.util.List;
import java.util.Collections;
import java.util.ArrayList;

/**
 * Contains a list of cities in a single tour
 * 
 * @author  Austin E. McGee
 */



public class Tour<T> implements Comparable<Tour<T>>
{
    private ArrayList tour;
    private Double totCost; // Total cost of the edges

    /**
     * Constructor for objects of class Tour
     * 
     * <b>Preconditions</b>:        Vertices must have been defined
     * <b>Postconditions</b>:       The tour has been created
     */
    public Tour(ArrayList list, double cost)
    {
        // initialise instance variables
        tour = list;
        totCost = cost * -1;
    }
    

    /**
     * Rerturns the tour
     * 
     * <b>Preconditions</b>:        The tour must have been created
     * <b>Postconditions</b>:       The tour has been returned
     * 
     * 
     * @return  tour                a List containing the tour    
     */
    public ArrayList getTour()
    {
        return tour;
    }
    
    
    /**
     * Rerturns the total cost
     * 
     * <b>Preconditions</b>:        The tour must have been created
     * <b>Postconditions</b>:       The tour's total cost has been returned
     * 
     * 
     * @return  totCost             a double containing the total cost    
     */
    public double getTotCost()
    {
        return totCost;
    }
    
    /**
     * Compares the total cost of the tours
     * 
     * <b>Preconditions</b>:        The tours must have been created and calculated
     * <b>Postconditions</b>:       The tours have been compared
     * 
     * 
     * @return               containing the smaller cost
     */
    public int compareTo(Tour<T> t)
    {
        int result;
        
        result = this.totCost.compareTo(t.totCost);
        return result;
    }
}
