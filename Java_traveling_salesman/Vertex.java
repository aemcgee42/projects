
/**
 * Holds a vertex
 * 
 * @Austin E. McGee
 */
public class Vertex<T> implements Comparable<Vertex<T>>
{
    private T key;  // Holds the search key
    private boolean visted = false; // Tells if it was visted
    private T fromKey; // Holds the vertex it came from
    private int index;  // Holds where it is in the HeapArray
    private Double cost; // Holds the total cost

    /**
     * Constructor for objects of class Vertex
     * <b>Preconditions</b>:        The graph must be created                       
     * <b>Postconditions</b>:       A vertex has been created
     * 
     * @param  key a T containing the key for the vertex
     *
     */
    public Vertex(T key)
    {
        this.key = key;
        cost = 0.0;
    }

    /**
     * Returns the vertex's key
     * <b>Preconditions</b>:        The vertex must be created with a valid key                       
     * <b>Postconditions</b>:       The key has been returned
     * 
     * @return  key a T containing the key for the vertex
     *
     */
    public T getKey()
    {
        return key;
    }

    /**
     * Returns true if the vertex has been visted, false if otherwise
     * <b>Preconditions</b>:        The vertex must be created with a valid key                       
     * <b>Postconditions</b>:       The key's visted status has been returned
     * 
     * @return  visted a boolean containing the true if true if the vertex has been visted, false if otherwise
     *
     */
    public boolean getVisted()
    {
        return visted;
    }

    /**
     * Updates the vertex if it has been visted or not
     * <b>Preconditions</b>:        The vertex must be created with a valid key                       
     * <b>Postconditions</b>:       The key's visted status has been changed
     * 
     * @param  change a boolean containing the true if true if the vertex has been visted, false if otherwise
     *
     */
    public void setVisted(boolean change)
    {
        visted = change;
    }
    
    /**
     * Returns the key that was used to get to this key
     * <b>Preconditions</b>:        Both nodes must have been created and defined
     * <b>Postconditions</b>:       The node's key2 value has been returned
     * 
     * @returns weight    a Double containing the node's weight
     * 
     */
    public T getFromKey()
    {
        return fromKey;
    }
    
    /**
     * Sets the node's key that was used to get to this key
     * <b>Preconditions</b>:        Both nodes must have been created and defined
     * <b>Postconditions</b>:       The node's key2 value has been changed
     * 
     * @param key2    a T containing the key that was used to get to this key
     * 
     */
    public void setFromKey(T fromKey)
    {
        this.fromKey = fromKey;
    }
    
    /**
     * Returns the node's index
     * <b>Preconditions</b>:        The node must be in a HeapArray
     * <b>Postconditions</b>:       The node's index may now be used
     * 
     * @returns index    an int containing the node's index
     * 
     */
    public int getIndex()
    {
        return index;
    }
    
    /**
     * Sets the node's index
     * <b>Preconditions</b>:        The node must be in a HeapArray
     * <b>Postconditions</b>:       The node's index has been changed
     * 
     * @param index    an int containing the node's new index
     * 
     */
    public void setIndex(int index)
    {
        this.index = index;
    }
    
    /**
     * Returns the node's cost
     * <b>Preconditions</b>:        The node must be in a HeapArray
     * <b>Postconditions</b>:       The node's cost may now be used
     * 
     * @returns cost    a Double containing the node's cost
     * 
     */
    public Double getCost()
    {
        return cost;
    }
    
    /**
     * Sets the node's cost
     * <b>Preconditions</b>:        The node must be in a HeapArray
     * <b>Postconditions</b>:       The node's cost has been changed
     * 
     * @param cost    a Double containing the node's new cost
     * 
     */
    public void setCost(Double cost)
    {
        this.cost = cost;
    }

    /**
     * Creates a String out of the information stored in the object
     * <b>Preconditions</b>:        The node must have been created and defined
     * <b>Postconditions</b>:       The node has been turned into a String
     * 
     * @returns String  containing the inoformation that is stored in the object
     * 
     */
    public String toString()
    {
        // Will display total cost so far up to that vertex
        return "" + key + ", " + cost + "\r\n";
    }
    
    
    /**
     * Compares the two Vertex objects
     * <b>Preconditions</b>:        The node must have been created and defined
     * <b>Postconditions</b>:       The node has been compared with another node
     * 
     * @returns     containing the smaller weight
     * 
     */
    public int compareTo(Vertex<T> v)
    {
        int result;
        //Double weight1 = this.getWeight();
        //Double weight2 = e.getWeight();

        result = this.cost.compareTo(v.cost);
        return result;
    }
}
