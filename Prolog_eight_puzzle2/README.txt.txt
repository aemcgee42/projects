Use Prolog to run this program.

Type eight_plan to run.

This program uses Prolog to solve the 8-puzzle problem. The initial state is defined in the file, so it must be changed if you want to start in a different state. The solution includes the directions used to move the empty space (here a 0) to the correct place (the bottom right of the board).